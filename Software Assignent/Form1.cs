﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Software_Assignent
{
    public partial class Form1 : Form
    {

        // Initializing the bit map to allow drawing within the picture box
        Bitmap OutputBitmap = new Bitmap(640, 480);
        Canvass MyCanvass;
        protected OpenFileDialog ofd = new OpenFileDialog();
        protected SaveFileDialog sfd = new SaveFileDialog();

        public Form1()
        {
            InitializeComponent();
            MyCanvass = new Canvass(Graphics.FromImage(OutputBitmap));
            //this.TopMost = true;
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        // this is a click event that will execute anything in the command area when execute is clicked
        private void ExecuteBtn_Click(object sender, EventArgs e)
        {
            String Command = CommandArea.Text.Trim().ToLower();
            if (Command.Equals("line") == true)
            {
                MyCanvass.DrawLine(50, 50);
                Console.WriteLine("LINE");
            }
            else if (Command.Equals("square") == true)
            {
                MyCanvass.DrawSquare(25);
                Console.WriteLine("LINE");
            }
            else if (Command.Equals("circle") == true)
            {
                MyCanvass.DrawEllipse(25);
                Console.WriteLine("LINE");
            }
            else if (Command.Equals("triangle") == true)
            {
                MyCanvass.DrawPolygon();
                Console.WriteLine("LINE");
            }
            CommandArea.Text = "";
            Refresh();
        }


        // click event that will clear anything on the screen, command areas and picture box 
        private void ClearBtn_Click(object sender, EventArgs e)
        {
            CommandArea.Text = "";
            CommandLine.Text = "";
            OutputBox.Image = null;
            this.Invalidate();
            Refresh();
            MessageBox.Show("Fields cleared");
        }

        // ket down ecent on the command line that will execute a command entered when enter ket is pressed
        private void CommandLine_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                String Command = CommandLine.Text.Trim().ToLower();
                if (Command.Equals("line") == true)
                {
                    MyCanvass.DrawLine(100, 100);
                    Console.WriteLine("LINE");
                }
                else if (Command.Equals("square") == true)
                {
                    MyCanvass.DrawSquare(50);
                    Console.WriteLine("LINE");
                }
                else if (Command.Equals("circle") == true)
                {
                    MyCanvass.DrawEllipse(50);
                    Console.WriteLine("LINE");
                }
                else if (Command.Equals("triangle") == true)
                {
                    MyCanvass.DrawPolygon();
                    Console.WriteLine("LINE");
                }
                CommandLine.Text = "";
                Refresh();
            }
        }

        // outputs the image to the bitmap 
        private void OutputBox_Paint(object sender, PaintEventArgs e)
        {
            Graphics g = e.Graphics;
            g.DrawImageUnscaled(OutputBitmap, 0, 0);
        }

        // line split on the command ares so more then more line of code can be run at one time
        private void CommandArea_KeyDown(object sender, KeyEventArgs e)
        {
            String line, command;
            String[] split;
            String[] parameters;
            int toX, toY;
            if (e.KeyCode == Keys.Enter)
            {
                Console.WriteLine("Enter key pressed");
                line = CommandArea.Text;
                split = line.Split(' ');
                Console.WriteLine(split[0]);
                Console.WriteLine(split[1]);
                command = split[0];
                parameters = split[1].Split(',');
                Console.WriteLine(parameters[0]);
                Console.WriteLine(parameters[1]);
                toX = Int32.Parse(parameters[0]);
                toY = Int32.Parse(parameters[1]);
            }
        }

        public void MoveTo(int x, int y)
        {
           
        }

        private void toolStripLabel1_Click(object sender, EventArgs e)
        {

        }

        private void SaveMenuBtn_Click(object sender, EventArgs e)
        {
            if (sfd.ShowDialog() == DialogResult.OK)
            {
                File.WriteAllText(sfd.FileName, CommandArea.Text);
            }
        }

        private void OpenMenuBtn_Click(object sender, EventArgs e)
        {
            if (ofd.ShowDialog() == DialogResult.OK)
            {
                foreach (String line in System.IO.File.ReadLines(ofd.FileName))
                {
                    CommandArea.Text += line + "\r\n";
                }
            }
        }
    }
}
