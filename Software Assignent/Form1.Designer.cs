﻿
namespace Software_Assignent
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.CommandArea = new System.Windows.Forms.TextBox();
            this.OutputBox = new System.Windows.Forms.PictureBox();
            this.ExecuteBtn = new System.Windows.Forms.Button();
            this.ClearBtn = new System.Windows.Forms.Button();
            this.CommandLine = new System.Windows.Forms.TextBox();
            this.toolStrip1 = new System.Windows.Forms.ToolStrip();
            this.SaveMenuBtn = new System.Windows.Forms.ToolStripButton();
            this.OpenMenuBtn = new System.Windows.Forms.ToolStripButton();
            ((System.ComponentModel.ISupportInitialize)(this.OutputBox)).BeginInit();
            this.toolStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // CommandArea
            // 
            this.CommandArea.Location = new System.Drawing.Point(12, 28);
            this.CommandArea.Multiline = true;
            this.CommandArea.Name = "CommandArea";
            this.CommandArea.Size = new System.Drawing.Size(354, 451);
            this.CommandArea.TabIndex = 0;
            this.CommandArea.KeyDown += new System.Windows.Forms.KeyEventHandler(this.CommandArea_KeyDown);
            // 
            // OutputBox
            // 
            this.OutputBox.Location = new System.Drawing.Point(372, 28);
            this.OutputBox.Name = "OutputBox";
            this.OutputBox.Size = new System.Drawing.Size(358, 451);
            this.OutputBox.TabIndex = 1;
            this.OutputBox.TabStop = false;
            this.OutputBox.Paint += new System.Windows.Forms.PaintEventHandler(this.OutputBox_Paint);
            // 
            // ExecuteBtn
            // 
            this.ExecuteBtn.Location = new System.Drawing.Point(12, 485);
            this.ExecuteBtn.Name = "ExecuteBtn";
            this.ExecuteBtn.Size = new System.Drawing.Size(75, 23);
            this.ExecuteBtn.TabIndex = 2;
            this.ExecuteBtn.Text = "Execute";
            this.ExecuteBtn.UseVisualStyleBackColor = true;
            this.ExecuteBtn.Click += new System.EventHandler(this.ExecuteBtn_Click);
            // 
            // ClearBtn
            // 
            this.ClearBtn.Location = new System.Drawing.Point(655, 485);
            this.ClearBtn.Name = "ClearBtn";
            this.ClearBtn.Size = new System.Drawing.Size(75, 23);
            this.ClearBtn.TabIndex = 3;
            this.ClearBtn.Text = "Clear";
            this.ClearBtn.UseVisualStyleBackColor = true;
            this.ClearBtn.Click += new System.EventHandler(this.ClearBtn_Click);
            // 
            // CommandLine
            // 
            this.CommandLine.Location = new System.Drawing.Point(214, 501);
            this.CommandLine.Name = "CommandLine";
            this.CommandLine.Size = new System.Drawing.Size(316, 20);
            this.CommandLine.TabIndex = 4;
            this.CommandLine.KeyDown += new System.Windows.Forms.KeyEventHandler(this.CommandLine_KeyDown);
            // 
            // toolStrip1
            // 
            this.toolStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.SaveMenuBtn,
            this.OpenMenuBtn});
            this.toolStrip1.Location = new System.Drawing.Point(0, 0);
            this.toolStrip1.Name = "toolStrip1";
            this.toolStrip1.Size = new System.Drawing.Size(744, 25);
            this.toolStrip1.TabIndex = 5;
            this.toolStrip1.Text = "toolStrip1";
            // 
            // SaveMenuBtn
            // 
            this.SaveMenuBtn.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.SaveMenuBtn.Image = ((System.Drawing.Image)(resources.GetObject("SaveMenuBtn.Image")));
            this.SaveMenuBtn.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.SaveMenuBtn.Name = "SaveMenuBtn";
            this.SaveMenuBtn.Size = new System.Drawing.Size(35, 22);
            this.SaveMenuBtn.Text = "Save";
            this.SaveMenuBtn.Click += new System.EventHandler(this.SaveMenuBtn_Click);
            // 
            // OpenMenuBtn
            // 
            this.OpenMenuBtn.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.OpenMenuBtn.Image = ((System.Drawing.Image)(resources.GetObject("OpenMenuBtn.Image")));
            this.OpenMenuBtn.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.OpenMenuBtn.Name = "OpenMenuBtn";
            this.OpenMenuBtn.Size = new System.Drawing.Size(40, 22);
            this.OpenMenuBtn.Text = "Open";
            this.OpenMenuBtn.Click += new System.EventHandler(this.OpenMenuBtn_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(744, 556);
            this.Controls.Add(this.toolStrip1);
            this.Controls.Add(this.CommandLine);
            this.Controls.Add(this.ClearBtn);
            this.Controls.Add(this.ExecuteBtn);
            this.Controls.Add(this.OutputBox);
            this.Controls.Add(this.CommandArea);
            this.Name = "Form1";
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.Form1_Load);
            ((System.ComponentModel.ISupportInitialize)(this.OutputBox)).EndInit();
            this.toolStrip1.ResumeLayout(false);
            this.toolStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox CommandArea;
        private System.Windows.Forms.PictureBox OutputBox;
        private System.Windows.Forms.Button ExecuteBtn;
        private System.Windows.Forms.Button ClearBtn;
        private System.Windows.Forms.TextBox CommandLine;
        private System.Windows.Forms.ToolStrip toolStrip1;
        private System.Windows.Forms.ToolStripButton SaveMenuBtn;
        private System.Windows.Forms.ToolStripButton OpenMenuBtn;
    }
}

