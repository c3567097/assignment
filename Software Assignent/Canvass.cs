﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Software_Assignent
{
    class Canvass
    {
        Graphics g;
        Pen Pen;
        int xPos, yPos;

        public Canvass(Graphics g)
        {
            this.g = g;
            xPos = yPos = 0;
            Pen = new Pen(Color.Black, 3);
            //Pen = new Pen(Color.Red, 3);
            //Pen = new Pen(Color.Green, 3);
            //Pen = new Pen(Color.Blue, 3);
        }

        public void DrawLine(int toX, int toY)
        {
            g.DrawLine(Pen, xPos, yPos, toX, toY);
            xPos = toX;
            yPos = toY;
        }

        public void DrawSquare(int width)
        {
            g.DrawRectangle(Pen, xPos, yPos, xPos + width, yPos + width);
        }

        public void DrawEllipse(int raduis)
        {
            g.DrawEllipse(Pen, xPos, yPos, xPos + raduis, yPos + raduis);
        }

        public void DrawPolygon()
        {
            // Create points that define polygon.
            PointF point1 = new PointF(50.0F, 50.0F);
            PointF point2 = new PointF(100.0F, 20.0F);
            PointF point3 = new PointF(150.0F, 50.0F);
            PointF[] curvePoints =
                     {
                 point1,
                 point2,
                 point3 
             };

            g.DrawPolygon(Pen, curvePoints);
        }
    }
}
